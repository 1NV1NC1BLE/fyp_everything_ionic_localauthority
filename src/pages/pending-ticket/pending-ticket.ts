import {Component} from '@angular/core';
import {
  AlertController,
  IonicPage,
  LoadingController,
  ModalController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import {ComplaintTicketProvider} from "../../providers/complaint-ticket/complaint-ticket";
import {TicketDescriptionPage} from "../ticket-description/ticket-description";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";


@IonicPage()
@Component({
  selector: 'page-pending-ticket',
  templateUrl: 'pending-ticket.html',
})
export class PendingTicketPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public modalCtrl: ModalController,
              public loadingCtrl: LoadingController,
              public pendingTicket: ComplaintTicketProvider) {

    this.reloadData();
  }

  presentDescriptionModal(index: number) {
    let modal = this.modalCtrl.create(TicketDescriptionPage, {indexNo: index});
    modal.present();
  }

  approveTicket(index: number) {

    let acceptTicket = () => {
      return new Promise((resolve, reject) => {
        let ticketID = this.pendingTicket.list[index].id;

        let mySettings = new Settings("complaint-ticket/approve-ticket");
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);

        myAjax.getWithParaMethod([ticketID], () => {
          resolve();
        }, () => {
          reject();
        });
      });
    };

    let alert = this.alertCtrl.create({
      title: 'Approve Ticket?',

      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Confirm',
          handler: () => {

            let loading = this.loadingCtrl.create({content: 'Processing...'});
            loading.present();

            acceptTicket().then(() => {
              this.displayAlert('Ticket Approved!', 'You may check the approved ticket at Approved Ticket Page.');
              this.reloadData();

            }, () => {
              this.displayToast();
            }).then(() => {
              loading.dismiss();
            });
          }
        }
      ]
    });
    alert.present();
  }

  rejectTicket(index: number) {

    let rejectTicket = () => {
      return new Promise((resolve, reject) => {
        let ticketID = this.pendingTicket.list[index].id;

        let mySettings = new Settings("complaint-ticket/reject-ticket");
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);

        myAjax.getWithParaMethod([ticketID], () => {
          resolve();
        }, () => {
          reject();
        });
      });
    };

    let alert = this.alertCtrl.create({
      title: 'Reject Ticket?',

      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Confirm',
          handler: () => {

            let loading = this.loadingCtrl.create({content: 'Processing...'});
            loading.present();

            rejectTicket().then(() => {
              this.displayAlert('Ticket Rejected!', 'You may check the rejected ticket at Rejected Ticket Page.');
              this.reloadData();

            }, () => {
              this.displayToast();
            }).then(() => {
              loading.dismiss();
            });
          }
        }
      ]
    });
    alert.present();

  }

  displayAlert(title: string, msg: string) {
    let alert = this.alertCtrl.create({
      title: title,
      message: msg,
      buttons: ['OK']
    });

    alert.present();
  }

  displayToast() {
    let toast = this.toastCtrl.create({
      message: "Something Bad Happened! Please try again later!",
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }


  reloadData() {
    this.pendingTicket.fetchAllRecords('complaint-ticket/pending-ticket');
  }

  doRefresh(refresher) {

    this.reloadData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
