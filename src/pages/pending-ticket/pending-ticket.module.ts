import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingTicketPage } from './pending-ticket';

@NgModule({
  declarations: [
    PendingTicketPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingTicketPage),
  ],
})
export class PendingTicketPageModule {}
