import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProjectDetailModalPage } from './edit-project-detail-modal';

@NgModule({
  declarations: [
    EditProjectDetailModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditProjectDetailModalPage),
  ],
})
export class EditProjectDetailModalPageModule {}
