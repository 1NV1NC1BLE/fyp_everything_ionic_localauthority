import {Component} from '@angular/core';
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ToastController,
  ViewController
} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MaintenanceProject} from "../../../../fyp_everything_modules/model/m_project/MaintenanceProject";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {ProjectStatusProvider} from "../../providers/project-status/project-status";


@IonicPage()
@Component({
  selector: 'page-edit-project-detail-modal',
  templateUrl: 'edit-project-detail-modal.html',
})
export class EditProjectDetailModalPage {

  private myProject: MaintenanceProject;
  public projectDetailFormGroup: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              public projectStatus: ProjectStatusProvider) {

    this.projectStatus.fetchAllRecords();
    this.myProject = this.navParams.get('project');

    this.projectDetailFormGroup = new FormGroup({

      title: new FormControl(this.myProject.title, [Validators.required]),
      description: new FormControl(this.myProject.description, [Validators.required]),
      status: new FormControl(this.myProject.status.id, Validators.required),
      budget: new FormControl(this.myProject.budget, [Validators.required]),
      startDate: new FormControl(this.myProject.startDate, [Validators.required]),
      endDate: new FormControl(this.myProject.endDate, [Validators.required])
    });
  }

  //todo input validation for start date and end date
  onSubmitProjectDetailEditForm() {

    let submitProjectDetail = () => {
      return new Promise((resolve, reject) => {
        let data = JSON.stringify(this.projectDetailFormGroup.getRawValue());

        let mySettings = new Settings('maintenance-project/update-detail');
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);
        myAjax.putMethod([this.myProject.id.toString()], data, () => {
          resolve();

        }, () => {
          reject();
        });
      });
    };

    let loading = this.loadingCtrl.create({content: 'Submitting'});
    loading.present();
    submitProjectDetail().then(() => {

      this.displayToast('Project Details Updated!');
      this.dismiss(true);

    }, () => {

      this.displayToast('Opps, something bad happened!');
      this.dismiss(false);

    }).then(()=>{
      loading.dismiss();
    });
  }

  displayToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  dismiss(isEdited: Boolean) {
    this.viewCtrl.dismiss(isEdited);
  }

}
