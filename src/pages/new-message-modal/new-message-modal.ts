import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";


@IonicPage()
@Component({
  selector: 'page-new-message-modal',
  templateUrl: 'new-message-modal.html',
})
export class NewMessageModalPage {

  public messageFormGroup: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public viewCtrl: ViewController) {

    this.messageFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required]),
      ticketID: new FormControl(this.navParams.get('ticketID'))
    })
  }

  onSubmitMessageForm() {

    let submitMessage = () => {
      return new Promise((resolve, reject) => {

        let data = JSON.stringify(this.messageFormGroup.getRawValue());
        let mySetting = new Settings('complaint-ticket/local-authority/messages/submit');
        mySetting.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySetting);
        myAjax.postMethod(data, () => {
          resolve();
        }, () => {
          reject();
        });
      });
    };

    let loading = this.loadingCtrl.create({content: 'Submitting'});
    loading.present();
    submitMessage().then(() => {

      this.presentToast('Message Submitted!');

    }, () => {

      this.presentToast('Opps, Something Bad Happened!');

    }).then(()=>{
      loading.dismiss();
      this.dismiss();
    });


  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

}
