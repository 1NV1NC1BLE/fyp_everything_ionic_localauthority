import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserComplaintTicket} from "../../../../fyp_everything_modules/model/complaint/UserComplaintTicket";
import * as $ from "jquery";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {LocalAuthorityProjectPage} from "../local-authority-project/local-authority-project";
import {DashboardPage} from "../dashboard/dashboard";

@IonicPage()
@Component({
  selector: 'page-create-maintenance-project',
  templateUrl: 'create-maintenance-project.html',
})
export class CreateMaintenanceProjectPage {

  private ticket: UserComplaintTicket;

  public ticketInfo: FormGroup;
  public projectScopeForm: FormGroup;
  public additionalInfoForm: FormGroup;
  public projectProgressForm: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController) {

    this.ticket = this.navParams.get('approvedTicket');

    this.ticketInfo = new FormGroup({
      ticketID: new FormControl('')
    });

    this.projectScopeForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

    this.additionalInfoForm = new FormGroup({
      budget: new FormControl('', [Validators.required, Validators.min(0)]),
      startDate: new FormControl('', Validators.required),
      endDate: new FormControl('', Validators.required)
    });

    this.projectProgressForm = new FormGroup({
      progressTitle: new FormControl(''),
      progressDes: new FormControl('')
    }, [this.checkProgressDesInput, this.checkProgressTitleInput]);

  }

  onSubmit() {

    let submitNewProject = () => {
      return new Promise((resolve, reject) => {

        if (this.ticket != null)
          this.ticketInfo.controls['ticketID'].setValue(this.ticket.id);

        let data = $.extend(this.ticketInfo.getRawValue(),
          this.projectScopeForm.getRawValue(),
          this.additionalInfoForm.getRawValue(),
          this.projectProgressForm.getRawValue());

        data = JSON.stringify(data);

        let mySettings = new Settings('maintenance-project/submit');
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);
        myAjax.postMethod(data, (response) => {

          resolve(response);
        }, (response) => {
          reject(response);
        });
      });
    };

    let loading = this.loadingCtrl.create({content: 'Submitting'});
    loading.present();

    submitNewProject().then(() => {
      let alert = this.alertCtrl.create({
        title: 'New Maintenance Project Created!',
        message: '',
        buttons: ['OK']
      });
      alert.present();

    }, () => {
      let toast = this.toastCtrl.create({
        message: "Something Bad Happened! Please try again later!",
        duration: 3000,
        position: 'bottom'
      });
      toast.present();

    }).then(() => {
      loading.dismiss();
      this.navCtrl.setRoot(DashboardPage);
    });

  }

  checkProgressTitleInput(group: FormGroup) {
    let title = group.controls.progressTitle.value;
    let description = group.controls.progressDes.value;

    if (title == '' && description != '')
      return {emptyTitle: true};
    else
      return null;

  }

  checkProgressDesInput(group: FormGroup) {
    let title = group.controls.progressTitle.value;
    let description = group.controls.progressDes.value;

    if (description == '' && title != '')
      return {emptyDes: true}
    else
      return null;

  }

}
