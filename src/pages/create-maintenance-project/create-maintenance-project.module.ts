import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateMaintenanceProjectPage } from './create-maintenance-project';

@NgModule({
  declarations: [
    CreateMaintenanceProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateMaintenanceProjectPage),
  ],
})
export class CreateMaintenanceProjectPageModule {}
