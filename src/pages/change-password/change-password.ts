import { Component } from '@angular/core';
import {ToastController, IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {LoginPage} from "../login/login";

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  public changePassFrmGroup:FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl:ToastController,
              private loadingCtrl: LoadingController) {

    this.changePassFrmGroup = new FormGroup({
      currentPassword: new FormControl('',Validators.required),
      newPassword: new FormControl('', [Validators.required,Validators.minLength(6)]),
      confirmNewPassword: new FormControl('', Validators.required)
    },this.checkPasswords);
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.newPassword.value;
    let confirmPass = group.controls.confirmNewPassword.value;

    return pass === confirmPass ? null : {mismatch: true}
  }

  onSubmitChangePasswordForm(){

    let changePassword = () => {
      return new Promise((resolve, reject) => {
        let mySetting = new Settings('profile/local-authority-staff/change-password');
        mySetting.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySetting);

        let formData = JSON.stringify(this.changePassFrmGroup.getRawValue());

        myAjax.postMethod(formData,(response)=>{

          resolve();
        },(response)=>{

          reject(response);
        });
      });
    };

    let loading = this.loadingCtrl.create({content:'Processing...'});
    loading.present();
    changePassword().then(()=>{

      this.presentToast('Password Changed! \n You are required to log in your account again!');
      this.navCtrl.setRoot(LoginPage);

    }, (response)=>{
      if(response.status == 400)
      {
        this.presentToast('Invalid current password!');
      }
    }).then(()=>{
      loading.dismiss();
    });

  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }
}
