import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {MaintenanceProjectProvider} from "../../providers/maintenance-project/maintenance-project";
import {ProjectProgressPage} from "../project-progress/project-progress";
import {ProjectFeedbackPage} from "../project-feedback/project-feedback";
import {ProjectDetailsPage} from "../project-details/project-details";

@IonicPage()
@Component({
  selector: 'page-public-request-project',
  templateUrl: 'public-request-project.html',
})
export class PublicRequestProjectPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private events: Events,
              public myMaintenanceProject: MaintenanceProjectProvider) {

    this.events.subscribe('reloadProjectPage',()=>{
      this.refreshData();
    });

    this.refreshData();
  }


  pushToProjectDetails(index: number) {
    let myProjectDetail = this.myMaintenanceProject.list[index];

    this.navCtrl.push(ProjectDetailsPage, {
      projectDetail: myProjectDetail,
    });
  }

  pushToProjectProgress(index: number) {
    let myProjectID = this.myMaintenanceProject.list[index].id;

    this.navCtrl.push(ProjectProgressPage, {
      projectID: myProjectID
    });
  }

  pushToProjectFeedback(index: number) {
    let myProjectID = this.myMaintenanceProject.list[index].id;
    this.navCtrl.push(ProjectFeedbackPage, {projectID: myProjectID});
  }

  refreshData() {
    this.myMaintenanceProject.fetchMaintenanceProjectList('maintenance-project/public-request-project');
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
