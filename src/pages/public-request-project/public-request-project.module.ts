import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicRequestProjectPage } from './public-request-project';

@NgModule({
  declarations: [
    PublicRequestProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicRequestProjectPage),
  ],
})
export class MyMaintenanceProjectPageModule {}
