import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import {LocalAuthorityStaffAccountInfoProvider} from "../../providers/local-authority-staff-account-info/local-authority-staff-account-info";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {RejectedTicketPage} from "../rejected-ticket/rejected-ticket";
import {LoginPage} from "../login/login";
import {ApprovedTicketPage} from "../approved-ticket/approved-ticket";
import {CreateMaintenanceProjectPage} from "../create-maintenance-project/create-maintenance-project";
import {StaffInfoPage} from "../staff-info/staff-info";
import {PendingTicketPage} from "../pending-ticket/pending-ticket";
import {PublicRequestProjectPage} from "../public-request-project/public-request-project";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {LocalAuthorityProjectPage} from "../local-authority-project/local-authority-project";

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public nav: NavController,
              public navParams: NavParams,
              public loadingCtrl:LoadingController,
              public alertCtrl: AlertController,
              public laStaffUser:LocalAuthorityStaffAccountInfoProvider) {

    this.laStaffUser.getProfileData();


  }


  openPendingTicketPage() {
    this.nav.push(PendingTicketPage);
  }

  openApprovedTicketPage() {
    this.nav.push(ApprovedTicketPage);
  }

  openRejectedTicketPage() {
    this.nav.push(RejectedTicketPage);
  }

  openCreateMaintenanceProjectPage() {
    this.nav.push(CreateMaintenanceProjectPage);
  }

  openStaffInfoPage() {
    this.nav.push(StaffInfoPage);
  }

  onLogout() {

    let loading = this.loadingCtrl.create({content: "Logging Out!"});

    let logout = () => {
      return new Promise((resolve) => {

        let mySettings = new Settings("logout");
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);

        myAjax.postMethod(() => {
          resolve();

        }, () => {
          resolve();
        });
      });
    };

    loading.present().then(() => {

      logout().then(() => {

        window.localStorage.removeItem('token');
        window.localStorage.removeItem('isRemember');
        this.nav.setRoot(LoginPage);

      }).then(() => {

        loading.dismiss();

      });
    });


  }

}
