import { Component } from '@angular/core';
import {IonicPage, ModalController, NavParams} from 'ionic-angular';
import {MessagesProvider} from "../../providers/message/message";
import {ComplaintTicketProvider} from "../../providers/complaint-ticket/complaint-ticket";
import {NewMessageModalPage} from "../new-message-modal/new-message-modal";


@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

  constructor(private navPar: NavParams,
              public modalCtrl:ModalController,
              public  mesProvider: MessagesProvider,
              public ticketProvider: ComplaintTicketProvider) {
    this.refreshData();
  }

  newMsg(){
    let ctID = this.ticketProvider.list[this.navPar.get('indexNo')].id;
    let modal = this.modalCtrl.create(NewMessageModalPage, {ticketID:ctID});
    modal.onDidDismiss(()=>{
      this.refreshData();
    });
    modal.present();
  }

  refreshData() {
    let ctID = this.ticketProvider.list[this.navPar.get('indexNo')];
    this.mesProvider.fetchAllRecords(ctID.id);
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
