import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RejectedTicketPage } from './rejected-ticket';

@NgModule({
  declarations: [
    RejectedTicketPage,
  ],
  imports: [
    IonicPageModule.forChild(RejectedTicketPage),
  ],
})
export class RejectedTicketPageModule {}
