import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {TicketDescriptionPage} from "../ticket-description/ticket-description";
import {ComplaintTicketProvider} from "../../providers/complaint-ticket/complaint-ticket";
import {MessagePage} from "../message/message";

/**
 * Generated class for the RejectedTicketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rejected-ticket',
  templateUrl: 'rejected-ticket.html',
})
export class RejectedTicketPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public rejectedTicket: ComplaintTicketProvider) {
    this.reloadData();
  }

  presentDescriptionModal(index: number) {
    let modal = this.modalCtrl.create(TicketDescriptionPage, {indexNo: index});
    modal.present();
  }


  showMessage(index: number) {
    this.navCtrl.push(MessagePage, {indexNo:index});
  }

  reloadData() {
    this.rejectedTicket.fetchAllRecords('complaint-ticket/rejected-ticket');
  }

  doRefresh(refresher) {

    this.reloadData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
