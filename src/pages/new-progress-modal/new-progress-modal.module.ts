import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewProgressModalPage } from './new-progress-modal';

@NgModule({
  declarations: [
    NewProgressModalPage,
  ],
  imports: [
    IonicPageModule.forChild(NewProgressModalPage),
  ],
})
export class NewProgressModalPageModule {}
