import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";


@IonicPage()
@Component({
  selector: 'page-new-progress-modal',
  templateUrl: 'new-progress-modal.html',
})
export class NewProgressModalPage {

  public progressFormGroup: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController) {

    this.progressFormGroup = new FormGroup({
      projectID: new FormControl(this.navParams.get('projectID'), Validators.required),
      progress: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

  }

  onSubmitProgressForm() {

    let submitProgress = () => {
      return new Promise((resolve, reject) => {
        let data = JSON.stringify(this.progressFormGroup.getRawValue());

        let mySettings = new Settings('maintenance-project/progress/submit');
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);
        myAjax.postMethod(data, () => {
            resolve();
          }, () => {
            reject();
          }
        );
      });
    };

    let loading = this.loadingCtrl.create({content: 'Submitting'});
    loading.present();

    submitProgress().then(() => {
      this.displayToast('New Project Progress Added!');

    }, () => {
      this.displayToast('Opps, something bad happened!');

    }).then(()=>{
      loading.dismiss();
      this.dismiss();
    });

  }

  displayToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
