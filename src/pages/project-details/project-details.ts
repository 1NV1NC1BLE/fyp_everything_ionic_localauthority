import { Component } from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {MaintenanceProject} from "../../../../fyp_everything_modules/model/m_project/MaintenanceProject";
import {EditProjectDetailModalPage} from "../edit-project-detail-modal/edit-project-detail-modal";
import {MaintenanceProjectProvider} from "../../providers/maintenance-project/maintenance-project";

@IonicPage()
@Component({
  selector: 'page-project-details',
  templateUrl: 'project-details.html',
})
export class ProjectDetailsPage {

  public myProject = new MaintenanceProject();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              private events: Events,
              public maintenanceProject: MaintenanceProjectProvider) {

    this.myProject = this.navParams.get('projectDetail');
  }

  editDetail(){

    let modal = this.modalCtrl.create(EditProjectDetailModalPage,{project: this.myProject});
    modal.onDidDismiss((isEdited)=>{

      if(isEdited){
        this.events.publish('reloadProjectPage');
        this.navCtrl.pop();
      }

    });
    modal.present();

  }
}
