import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ComplaintTicketProvider} from "../../providers/complaint-ticket/complaint-ticket";
import {TicketDescriptionPage} from "../ticket-description/ticket-description";
import {MessagePage} from "../message/message";
import {CreateMaintenanceProjectPage} from "../create-maintenance-project/create-maintenance-project";


@IonicPage()
@Component({
  selector: 'page-approved-ticket',
  templateUrl: 'approved-ticket.html',
})
export class ApprovedTicketPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl:ModalController,
              public approvedTicket: ComplaintTicketProvider) {
    this.reloadData();
  }

  pushCreateMaintenanceProjectPage(index:number){
    let ticket = this.approvedTicket.list[index];
    this.navCtrl.push(CreateMaintenanceProjectPage,{approvedTicket:ticket});
  }

  presentDescriptionModal(index:number) {
    let modal = this.modalCtrl.create(TicketDescriptionPage, {indexNo:index});
    modal.present();
  }


  showMessage(index:number){
    this.navCtrl.push(MessagePage, {indexNo:index});
  }

  reloadData(){
    this.approvedTicket.fetchAllRecords('complaint-ticket/approved-ticket');
  }

  doRefresh(refresher) {

    this.reloadData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
