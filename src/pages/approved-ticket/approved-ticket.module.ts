import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApprovedTicketPage } from './approved-ticket';

@NgModule({
  declarations: [
    ApprovedTicketPage,
  ],
  imports: [
    IonicPageModule.forChild(ApprovedTicketPage),
  ],
})
export class ApprovedTicketPageModule {}
