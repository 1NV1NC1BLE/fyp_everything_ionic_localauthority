import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {LocalAuthorityStaffAccountInfoProvider} from "../../providers/local-authority-staff-account-info/local-authority-staff-account-info";
import {LocalAuthorityInfoPage} from "../local-authority-info/local-authority-info";
import {ChangePasswordPage} from "../change-password/change-password";


@IonicPage()
@Component({
  selector: 'page-staff-info',
  templateUrl: 'staff-info.html',
})
export class StaffInfoPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public staffInfo:LocalAuthorityStaffAccountInfoProvider) {

  }

  openLocalAuthorityInfoModal(){
    let modal = this.modalCtrl.create(LocalAuthorityInfoPage);
    modal.present();
  }

  pushChangePasswordPage(){
    this.navCtrl.push(ChangePasswordPage);
  }

}
