import { Component } from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {ComplaintTicketProvider} from "../../providers/complaint-ticket/complaint-ticket";
import {UserComplaintTicket} from "../../../../fyp_everything_modules/model/complaint/UserComplaintTicket";


@IonicPage()
@Component({
  selector: 'page-ticket-description',
  templateUrl: 'ticket-description.html',
})
export class TicketDescriptionPage {

  public complaintTicket = new UserComplaintTicket();

  constructor(public viewCtrl: ViewController,
              public navParams: NavParams,
              public pendingTicket:ComplaintTicketProvider) {

    this.complaintTicket = this.pendingTicket.list[navParams.get('indexNo')];

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
