import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketDescriptionPage } from './ticket-description';

@NgModule({
  declarations: [
    TicketDescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(TicketDescriptionPage),
  ],
})
export class TicketDescriptionPageModule {}
