import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ProjectProgressProvider} from "../../providers/project-progress/project-progress";
import {NewProgressModalPage} from "../new-progress-modal/new-progress-modal";


@IonicPage()
@Component({
  selector: 'page-project-progress',
  templateUrl: 'project-progress.html',
})
export class ProjectProgressPage {

  private projectID;

  constructor(public navCtrl: NavController,
              public navParam: NavParams,
              public modalCtrl: ModalController,
              public progressProvider: ProjectProgressProvider) {

    this.projectID = this.navParam.get('projectID');

    this.refreshData();
  }

  displayAddProjectProgressModal(){
    let modal = this.modalCtrl.create(NewProgressModalPage, {projectID:this.projectID});
    modal.onDidDismiss(()=>{
      this.progressProvider.fetchAllRecords(this.projectID);
    });
    modal.present();
  }

  refreshData() {

    this.progressProvider.fetchAllRecords(this.projectID);
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);

  }

}
