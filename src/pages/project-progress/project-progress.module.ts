import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectProgressPage } from './project-progress';

@NgModule({
  declarations: [
    ProjectProgressPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectProgressPage),
  ],
})
export class ProjectProgressPageModule {}
