import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {LocalAuthorityStaffAccountInfoProvider} from "../../providers/local-authority-staff-account-info/local-authority-staff-account-info";



@IonicPage()
@Component({
  selector: 'page-local-authority-info',
  templateUrl: 'local-authority-info.html',
})
export class LocalAuthorityInfoPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public staffInfo:LocalAuthorityStaffAccountInfoProvider) {

  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
