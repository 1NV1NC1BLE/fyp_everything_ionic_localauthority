import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalAuthorityInfoPage } from './local-authority-info';

@NgModule({
  declarations: [
    LocalAuthorityInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalAuthorityInfoPage),
  ],
})
export class LocalAuthorityInfoPageModule {}
