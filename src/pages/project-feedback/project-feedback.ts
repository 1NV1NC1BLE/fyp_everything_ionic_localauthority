import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProjectFeedbackProvider} from "../../providers/project-feedback/project-feedback";

@IonicPage()
@Component({
  selector: 'page-project-feedback',
  templateUrl: 'project-feedback.html',
})
export class ProjectFeedbackPage {

  private projectID;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public feedbackProvider: ProjectFeedbackProvider) {

    this.projectID = this.navParams.get('projectID');
    this.refreshData();
  }

  refreshData() {
    this.feedbackProvider.fetchAllRecords(this.projectID);
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
