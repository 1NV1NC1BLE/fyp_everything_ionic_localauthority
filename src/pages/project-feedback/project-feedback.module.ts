import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectFeedbackPage } from './project-feedback';

@NgModule({
  declarations: [
    ProjectFeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectFeedbackPage),
  ],
})
export class ProjectFeedbackPageModule {}
