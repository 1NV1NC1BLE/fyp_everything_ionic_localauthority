import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalAuthorityProjectPage } from './local-authority-project';

@NgModule({
  declarations: [
    LocalAuthorityProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalAuthorityProjectPage),
  ],
})
export class LocalAuthorityProjectPageModule {}
