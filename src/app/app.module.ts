import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Ionic2RatingModule } from "ionic2-rating";

import { MyApp } from './app.component';

import { LoginPage } from "../pages/login/login";
import { DashboardPage } from "../pages/dashboard/dashboard";
import {PendingTicketPage} from "../pages/pending-ticket/pending-ticket";
import {ApprovedTicketPage} from "../pages/approved-ticket/approved-ticket";
import {RejectedTicketPage} from "../pages/rejected-ticket/rejected-ticket";
import {CreateMaintenanceProjectPage} from "../pages/create-maintenance-project/create-maintenance-project";
import {PublicRequestProjectPage} from "../pages/public-request-project/public-request-project";
import {LocalAuthorityProjectPage} from "../pages/local-authority-project/local-authority-project";
import {TicketDescriptionPage} from "../pages/ticket-description/ticket-description";
import {MessagePage} from "../pages/message/message";
import {NewMessageModalPage} from "../pages/new-message-modal/new-message-modal";
import {ProjectDetailsPage} from "../pages/project-details/project-details";
import {ProjectProgressPage} from "../pages/project-progress/project-progress";
import {ProjectFeedbackPage} from "../pages/project-feedback/project-feedback";
import {EditProjectDetailModalPage} from "../pages/edit-project-detail-modal/edit-project-detail-modal";
import {NewProgressModalPage} from "../pages/new-progress-modal/new-progress-modal";

import { ComplaintTicketProvider } from '../providers/complaint-ticket/complaint-ticket';
import { MessagesProvider } from '../providers/message/message';
import { ProjectStatusProvider } from '../providers/project-status/project-status';
import { LocalAuthorityStaffAccountInfoProvider } from '../providers/local-authority-staff-account-info/local-authority-staff-account-info';
import { MaintenanceProjectProvider } from '../providers/maintenance-project/maintenance-project';
import { ProjectProgressProvider } from '../providers/project-progress/project-progress';
import { ProjectFeedbackProvider } from '../providers/project-feedback/project-feedback';

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material";
import {StaffInfoPage} from "../pages/staff-info/staff-info";
import {ChangePasswordPage} from "../pages/change-password/change-password";
import {LocalAuthorityInfoPage} from "../pages/local-authority-info/local-authority-info";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DashboardPage,
    PendingTicketPage,
    ApprovedTicketPage,
    RejectedTicketPage,
    CreateMaintenanceProjectPage,
    PublicRequestProjectPage,
    LocalAuthorityProjectPage,
    TicketDescriptionPage,
    MessagePage,
    NewMessageModalPage,
    ProjectDetailsPage,
    ProjectProgressPage,
    ProjectFeedbackPage,
    EditProjectDetailModalPage,
    NewProgressModalPage,
    StaffInfoPage,
    LocalAuthorityInfoPage,
    ChangePasswordPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatStepperModule,
    MatFormFieldModule,
    MatIconModule,
    Ionic2RatingModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DashboardPage,
    PendingTicketPage,
    ApprovedTicketPage,
    RejectedTicketPage,
    CreateMaintenanceProjectPage,
    PublicRequestProjectPage,
    LocalAuthorityProjectPage,
    TicketDescriptionPage,
    MessagePage,
    NewMessageModalPage,
    ProjectDetailsPage,
    ProjectProgressPage,
    ProjectFeedbackPage,
    EditProjectDetailModalPage,
    NewProgressModalPage,
    StaffInfoPage,
    LocalAuthorityInfoPage,
    ChangePasswordPage
  ],

  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocalAuthorityStaffAccountInfoProvider,
    ComplaintTicketProvider,
    MessagesProvider,
    MaintenanceProjectProvider,
    ProjectProgressProvider,
    ProjectFeedbackProvider,
    ProjectStatusProvider,

  ]
})
export class AppModule {}
