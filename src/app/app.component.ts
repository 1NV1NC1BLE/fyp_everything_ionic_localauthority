import {Component, ViewChild} from '@angular/core';
import {Nav, Platform, LoadingController, ToastController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {LoginPage} from "../pages/login/login";
import {DashboardPage} from "../pages/dashboard/dashboard";
import {PendingTicketPage} from "../pages/pending-ticket/pending-ticket";
import {ApprovedTicketPage} from "../pages/approved-ticket/approved-ticket";
import {RejectedTicketPage} from "../pages/rejected-ticket/rejected-ticket";
import {CreateMaintenanceProjectPage} from "../pages/create-maintenance-project/create-maintenance-project";
import {PublicRequestProjectPage} from "../pages/public-request-project/public-request-project";
import {LocalAuthorityProjectPage} from "../pages/local-authority-project/local-authority-project";

import {LocalAuthorityStaffAccountInfoProvider} from "../providers/local-authority-staff-account-info/local-authority-staff-account-info";

import {Settings} from "../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../fyp_everything_modules/http/AjaxHandler";
import {StaffInfoPage} from "../pages/staff-info/staff-info";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              public toastCtrl:ToastController,
              public loadingCtrl: LoadingController,
              public laStaffUser: LocalAuthorityStaffAccountInfoProvider) {

    let autoLoginPromise = () => {
      return new Promise((resolve) => {

        let mySetting = new Settings('autoLogin');
        mySetting.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySetting);
        myAjax.getMethod((response) => {
          this.rootPage = DashboardPage;
          resolve();
        }, () => {
          this.rootPage = LoginPage;
          resolve();
        });

      });
    };

    platform.ready().then(() => {

      if (window.localStorage.getItem('isRemember') == 'true') {
        autoLoginPromise();
      }
      else {
        this.rootPage = LoginPage;
      }

    }).then(() => {
      if (!document.URL.startsWith('http')) {

        statusBar.styleLightContent();
        statusBar.backgroundColorByHexString('#002700');
        splashScreen.hide();
      }
    });

  }



  openDashBoardPage() {
    this.nav.setRoot(DashboardPage);
  }

  openPendingTicketPage() {
    this.nav.setRoot(PendingTicketPage);
  }

  openApprovedTicketPage() {
    this.nav.setRoot(ApprovedTicketPage);
  }

  openRejectedTicketPage() {
    this.nav.setRoot(RejectedTicketPage);
  }

  openCreateMaintenanceProjectPage() {
    this.nav.push(CreateMaintenanceProjectPage);
  }

  openMyMaintenanceProjectPage() {
    this.nav.setRoot(PublicRequestProjectPage);
  }

  openLocalAuthorityProjectPage() {
    this.nav.setRoot(LocalAuthorityProjectPage);
  }

  openStaffInfoPage() {
    this.nav.setRoot(StaffInfoPage);
  }

  onLogout() {

    let loading = this.loadingCtrl.create({content: "Logging Out!"});

    let logout = () => {
      return new Promise((resolve) => {

        let mySettings = new Settings("logout");
        mySettings.token = window.localStorage.getItem('token');

        let myAjax = new AjaxHandler(mySettings);

        myAjax.postMethod(() => {
          resolve();

        }, () => {
          resolve();
        });
      });
    };

    loading.present().then(() => {

      logout().then(() => {

        window.localStorage.removeItem('token');
        window.localStorage.removeItem('isRemember');
        this.nav.setRoot(LoginPage);

      }).then(() => {

        loading.dismiss();

      });
    });

  }

}



