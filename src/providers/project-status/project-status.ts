import { Injectable } from '@angular/core';

import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {ProjectStatus} from "../../../../fyp_everything_modules/model/m_project/ProjectStatus";

@Injectable()
export class ProjectStatusProvider {

  public list = [];

  constructor() {

  }

  fetchAllRecords() {

    let me = this;

    let mySettings = new Settings('project-status/index');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getMethod(function (response) {

      me.list = [];
      for (let i = 0; i < response.length; i++) {
        let temp = new ProjectStatus();
        temp.mapData(response[i]);
        me.list.push(temp);
      }

    });
  }
}
