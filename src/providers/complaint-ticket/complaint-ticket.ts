import {Injectable} from '@angular/core';
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {UserComplaintTicket} from "../../../../fyp_everything_modules/model/complaint/UserComplaintTicket";

@Injectable()
export class ComplaintTicketProvider {

  public list = [];

  constructor() {

  }

  fetchAllRecords(url: string) {

    this.list = [];

    let mySettings = new Settings(url);

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);

    myAjax.getMethod((response) => {

      for (let i = 0; i < response.length; i++) {

        let temp = new UserComplaintTicket();
        temp.mapData(response[i]);
        this.list.push(temp);
      }

    });
  }

  findItem(id: number): any {
    return Object.keys(this.list)[id];

  }
}
