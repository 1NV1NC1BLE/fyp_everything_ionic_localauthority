import { Injectable } from '@angular/core';
import {ProjectFeedback} from "../../../../fyp_everything_modules/model/m_project/ProjectFeedback";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";

@Injectable()
export class ProjectFeedbackProvider {

  public totalFeedback;
  public avgRating;
  public feedbackList = [];

  constructor() {

  }

  fetchAllRecords(projectID) {

    let mySettings = new Settings('maintenance-project/feedback');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getWithParaMethod([projectID], (response) => {

      this.avgRating = 0;
      this.feedbackList = [];
      this.totalFeedback = 0;

      this.loadOverallInfo(response.overallInfo[0]);

      if(response.userFeedbackList != null)
      {
        this.loadFeedbackList(response.userFeedbackList);
      }

    });
  }

  private loadOverallInfo(data) {
    this.avgRating = data.avgRating;
    this.totalFeedback = data.noReview;
  }

  private loadFeedbackList(data) {
    this.feedbackList = [];

    for (let i = 0; i < data.length; i++) {
      let temp = new ProjectFeedback();
      temp.mapData(data[i]);

      this.feedbackList.push(temp);
    }
  }

}
