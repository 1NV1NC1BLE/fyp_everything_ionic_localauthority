import { Injectable } from '@angular/core';
import {LocalAuthorityStaff} from "../../../../fyp_everything_modules/model/users/LocalAuthority";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";

@Injectable()
export class LocalAuthorityStaffAccountInfoProvider {

  localAuthorityStaff = new LocalAuthorityStaff();

  constructor() {

  }

  getProfileData() {

    let mySetting = new Settings('profile/local-authority-staff');
    mySetting.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySetting);
    myAjax.getMethod((response) => {
      this.localAuthorityStaff.mapData(response[0]);

    }, () => {

    });
  }
}
