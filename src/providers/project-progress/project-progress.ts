import { Injectable } from '@angular/core';
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {ProjectProgress} from "../../../../fyp_everything_modules/model/m_project/ProjectProgress";

@Injectable()
export class ProjectProgressProvider {

  public list = [];

  constructor() {

  }

  fetchAllRecords(projectID) {

    let mySettings = new Settings('maintenance-project/progress');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getWithParaMethod([projectID],  (response) => {

      this.list = [];
      for (let i = 0; i < response.length; i++) {

        let temp = new ProjectProgress();
        temp.mapData(response[i]);
        this.list.push(temp);

      }

    });
  }

}
