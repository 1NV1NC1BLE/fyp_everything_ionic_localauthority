
import { Injectable } from '@angular/core';
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {Message} from "../../../../fyp_everything_modules/model/complaint/Message";


@Injectable()
export class MessagesProvider {

  public list = [];

  constructor() {

  }

  fetchAllRecords(ctID) {

    let me = this;

    let mySettings = new Settings('complaint-ticket/local-authority/messages');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getWithParaMethod([ctID],function (response) {

      me.list = [];
      for (let i = 0; i < response.length; i++) {
        let temp = new Message();
        temp.mapData(response[i]);
        me.list.push(temp);
      }

    });
  }

}
